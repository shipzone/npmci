/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@shipzone/npmci',
  version: '4.1.4',
  description: 'node and docker in gitlab ci on steroids'
}
