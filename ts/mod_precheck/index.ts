import * as plugins from './plugins.js';
import * as paths from '../npmci.paths.js';
import { logger } from '../npmci.logging.js';
import { Npmci } from '../npmci.classes.npmci.js';

export const handleCli = async (npmciRefArg: Npmci, argvArg: any) => {
  logger.log('info', 'checking execution context');
  const presentRunnerTags = process.env.CI_RUNNER_TAGS.split(',').map((stringArg) =>
    stringArg.trim()
  );
  let allDesiredGitlabRunnerTagsPresent = true;
  for (const desiredRunnerTag of npmciRefArg.npmciConfig.getConfig().gitlabRunnerTags) {
    if (!presentRunnerTags.includes(desiredRunnerTag)) {
      allDesiredGitlabRunnerTagsPresent = false;
      logger.log(
        'error',
        `Desired runnerRag ${desiredRunnerTag} is missing in current execution context.`
      );
    }
  }
  if (!allDesiredGitlabRunnerTagsPresent) {
    process.exit(1);
  }
};
